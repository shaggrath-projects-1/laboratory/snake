const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const SRC_PATH = path.resolve(`${__dirname}/`, 'src');
const DIST_PATH = path.resolve(`${__dirname}/`, 'public');
const DEVELOPMENT = 'development';
const PRODUCTION = 'production';

module.exports = {
  name: 'snake',
  devtool: 'source-map',
  target: 'web',
  mode: DEVELOPMENT,
  entry: {
    index: [ path.resolve(SRC_PATH, 'index.tsx') ],
  },
  output: {
    filename: '[name].bundle.js',
    path: DIST_PATH,
    publicPath: '/',
  },
  resolve: {
    extensions: [ '.js', '.ts', '.tsx', '.json' ],
    modules: [
      'node_modules',
      SRC_PATH,
      '.',
    ],
    symlinks: false,
    cacheWithContext: false,
  },
  devServer: {
    compress: true,
    host: '0.0.0.0',
    port: 8181,
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      filename: 'index.html',
      publicPath: '',
      template: path.resolve(SRC_PATH, 'index.pug'),
    }),
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.pug$/i,
        loader: 'pug-loader',
      },
      {
        test: /\.scss$/i,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
            },
          },
          'sass-loader',
        ],
      },
    ],
  }
}
