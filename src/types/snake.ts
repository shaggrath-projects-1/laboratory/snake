export enum SnakeDirection {
  UP,
  RIGHT,
  DOWN,
  LEFT,
}


export type SnakeSectionSizeType = {
  width: number;
  height: number;
}

export type SnakeBodySegmentType = {
  x: number;
  y: number;
  width: number;
  height: number;
}
