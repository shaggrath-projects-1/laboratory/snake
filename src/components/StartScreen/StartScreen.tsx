import React from 'react';
import type { FC, ReactElement } from 'react';
import cx from 'classnames';

import Button from 'components/Button';
import type Game from 'models/Game';
import styles from './StartScreen.scss';

type Props = {
  className?: string;
  onStartGame?: () => void;
  game: Game;
};

const StartScreen: FC<Props> = (props): ReactElement => {
  const {
    className,
    game,
    onStartGame = () => {
    },
  } = props;

  return (
    <article
      className={cx(className, styles.StartScreen)}
    >
      <span className={cx(styles.StartScreen__score)}>
        {`Рекорд: ${game.record}`}
      </span>
      <Button color="primary" onClick={onStartGame}>
        Старт
      </Button>
    </article>
  );
};

export default StartScreen;
