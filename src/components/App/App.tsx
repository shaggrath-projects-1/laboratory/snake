import type { FC, ReactElement } from 'react';
import React, { useState } from 'react';
import cx from 'classnames';

import StartScreen from 'components/StartScreen';
import GameScene from 'components/GameScene';
import Game from 'models/Game';
import { GameStatus } from 'types/game';

import styles from './App.scss';

const App: FC = (): ReactElement => {
  const [ gameStatus, setGameStatus ] = useState<GameStatus>(GameStatus.STOP);
  const windowWidth = window.innerWidth;
  const windowHeight = window.innerHeight;
  const game = new Game(windowWidth, windowHeight);

  const onStartGame = () => {
    setGameStatus(GameStatus.START);
    game.start();
  };
  const onEndGame = () => {
    setGameStatus(GameStatus.STOP);
    game.end();
  };

  return (
    <main className={cx(styles.App)}>
      {
        gameStatus === GameStatus.START
          ? (<GameScene game={game} onEndGame={onEndGame}/>)
          : (<StartScreen game={game} onStartGame={onStartGame}/>)
      }
    </main>
  );
};

export default App;
