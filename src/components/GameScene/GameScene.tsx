import React, { useRef, useEffect } from 'react';
import type { FC, ReactElement, ReactDOM } from 'react';
import cx from 'classnames';

import type Game from 'models/Game';
import Snake from 'models/Snake';
import Drawer from 'models/Drawer';
import Engine from 'models/Engine';
import styles from './GameScene.scss';

type Props = {
  className?: string;
  onEndGame?: () => void;
  game: Game;
};

const GameScene: FC<Props> = (props): ReactElement => {
  const { className, game } = props;
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const snake = new Snake(game);
  const controlHandler = (event: KeyboardEvent) => {
    if(event.code === 'ArrowUp') {
      snake.goUp()
    }
    if(event.code === 'ArrowDown') {
      snake.goDown()
    }
    if(event.code === 'ArrowLeft') {
      snake.goLeft()
    }
    if(event.code === 'ArrowRight') {
      snake.goRight()
    }
  }

  useEffect((): () => void => {
    if (!canvasRef.current) {
      return () => {};
    }

    const drawer = new Drawer(canvasRef.current);
    const engine = new Engine(game, snake, drawer);
    document.addEventListener('keyup', controlHandler);

    return () => {
      document.removeEventListener('keyup', controlHandler);
    }
  }, []);

  return (
    <article
      className={cx(className, styles.GameScene)}
    >
      <canvas
        className={styles.GameScene__canvas}
        ref={canvasRef}
        width={game.fieldWidth}
        height={game.fieldHeight}
      />
    </article>
  );
};

export default GameScene;
