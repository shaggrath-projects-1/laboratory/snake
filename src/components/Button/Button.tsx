import React from 'react';
import type { FC, ReactElement, ReactNode } from 'react';
import cx from 'classnames';

import styles from './Button.scss';

type Props = {
  className?: string;
  children: ReactNode;
  onClick?: () => void;
  color?: 'primary' | 'success' | 'error' | 'warning';
};

const Button: FC<Props> = (props): ReactElement => {
  const {
    className,
    children,
    color = 'primary',
    onClick,
  } = props;

  return (
    <div
      role="button"
      onClick={onClick}
      className={
        cx(
          className,
          styles.Button,
          styles[`Button--${color}`],
        )}
    >
      {children}
    </div>
  );
};

export default Button;
