export default class Drawer {
  private readonly _ctx: CanvasRenderingContext2D | null | undefined;
  private _canvas: HTMLCanvasElement;

  constructor(canvas: HTMLCanvasElement) {
    this._canvas = canvas;
    this._ctx = canvas.getContext('2d');
    this.clear();
  }

  clear(): void {
    if (!this._ctx) {
      return;
    }

    this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
  }

  drawSegment(x: number, y: number, width: number, height: number, color: string = '#000000'): void {
    if (!this._ctx) {
      return;
    }

    this._ctx.fillStyle = color;
    this._ctx.fillRect(x, y, width, height);
  };
}
