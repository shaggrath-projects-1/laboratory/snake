import type Game from './/Game';
import { SnakeBodySegmentType, SnakeDirection, SnakeSectionSizeType } from '../types/snake';

export default class Snake {
  static COEFFICIENT = 3
  private _game: Game;
  private _direction: SnakeDirection = SnakeDirection.RIGHT;
  private readonly _sectionSize: SnakeSectionSizeType;
  private _body: SnakeBodySegmentType[];
  private _length: number = 6;

  constructor(game: Game) {
    this._game = game;
    this._sectionSize = {
      height: this._game.fieldHeight * (Snake.COEFFICIENT / 100),
      width: this._game.fieldWidth * (Snake.COEFFICIENT / 100),
    };
    this._body = [
      {
        x: Math.floor(((this._game.fieldWidth / this._sectionSize.width) * 0.5) * this._sectionSize.width),
        y: Math.floor(((this._game.fieldHeight / this._sectionSize.height) * 0.5) * this._sectionSize.height),
        ...this._sectionSize,
      }
    ];
  }

  get body(): SnakeBodySegmentType[] {
    return this._body;
  }

  get sectionSize(): SnakeSectionSizeType {
    return this._sectionSize;
  }

  public goDown(): void {
    this._direction = SnakeDirection.DOWN;
  }

  public goUp(): void {
    this._direction = SnakeDirection.UP;
  }

  public goLeft(): void {
    this._direction = SnakeDirection.LEFT;
  }

  public goRight(): void {
    this._direction = SnakeDirection.RIGHT;
  }

  public step(): void {
    const tempBody: SnakeBodySegmentType[] = [...this._body];
    const currentX: number = this._body[ this._body.length - 1 ].x;
    const currentY: number = this._body[ this._body.length - 1 ].y;
    switch (this._direction) {
      case SnakeDirection.RIGHT:
        tempBody.push({ y: currentY, x: currentX + this._sectionSize.width, ...this._sectionSize });
        break;
      case SnakeDirection.DOWN:
        tempBody.push({ y: currentY + this._sectionSize.height, x: currentX, ...this._sectionSize });
        break;
      case SnakeDirection.LEFT:
        tempBody.push({ y: currentY, x: currentX - this._sectionSize.width, ...this._sectionSize });
        break;
      case SnakeDirection.UP:
        tempBody.push({ y: currentY - this._sectionSize.height, x: currentX, ...this._sectionSize });
        break;
    }
    if(tempBody.length > this._length) {
      tempBody.shift();
    }

    this._body = [...tempBody];
    // one step for snake
  }
}
