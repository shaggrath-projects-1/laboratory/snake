import { GameStatus } from '../types/game';

export default class Game {
  private readonly _recordKey = 'snake-record';
  private readonly _windowWidth: number = Infinity;
  private readonly _windowHeight: number = Infinity;
  private _status: GameStatus = GameStatus.STOP;
  private _score: number = 0;

  constructor(width: number, height: number) {
    this._windowWidth = width;
    this._windowHeight = height;
  }

  get record():number {
    return Number(localStorage.getItem(this._recordKey)) || 0;
  }

  set record(val: number) {
    localStorage.setItem(this._recordKey, String(val));
  }

  get fieldWidth():number {
    return this._windowHeight - 100;
  }

  get fieldHeight():number {
    return this._windowHeight - 100;
  }

  get score(): number {
    return this._score;
  }

  get status(): GameStatus {
    return this._status;
  }

  set score(val) {
    if (this.record < val) {
      this.record = val;
    }
    this._score = val;
  }

  public start(): void {
    this._status = GameStatus.START;
  }

  public end(): void {
    this._status = GameStatus.STOP;
  }
}
