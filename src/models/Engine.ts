import type Game from 'models/Game';
import type Snake from 'models/Snake';
import Drawer from 'models/Drawer';
import { GameStatus } from 'types/game';

export default class Engine {
  private _game: Game;
  private _snake: Snake;
  private _drawer: Drawer;
  private readonly _tickID: NodeJS.Timer;

  constructor(game: Game, snake: Snake, drawer: Drawer) {
    this._game = game;
    this._snake = snake;
    this._drawer = drawer;

    snake.body.map(section => {
      this._drawer.drawSegment(section.x, section.y, section.width, section.height);
    })
    this.frameStep();
    this._tickID = setInterval(() => {
      // if (this._game.status === GameStatus.STOP) {
      //   clearInterval(this._tickID);
      // }
      this._snake.step();
    }, 100);
  }

  private frameStep = (): void => {
    const colors: string[] = [
      '#FF00FF',
      '#800080',
      '#FF0000',
      '#800000',
      '#FFFF00',
      '#00FF00',
    ]
    const generator = function* (arr: string[]) {
      let i = 0
      let length = arr.length
      while(true) {
        yield arr[i]
        i++
        if(i === length) {
          i = 0
        }
      }
    }
    const arrayLoop = generator(colors);
    this._drawer.clear();
    this._snake.body.map((section, index) => {
      // @ts-ignore
      this._drawer.drawSegment(section.x, section.y, section.width, section.height, arrayLoop.next().value);
    })
    // if (this._game.status === GameStatus.START) {
      requestAnimationFrame(this.frameStep);
    // }
  }
}
